
      program conversion_temps
              implicit none
              integer :: seconde
              integer,parameter :: min_to_sec = 60

              print *, "Combien de seconde(s) ?"
              read *, seconde
              print "(A,I0,A)", "Cela fait ", seconde/min_to_sec, " minute(s)"

      end program
