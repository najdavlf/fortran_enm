
      program conversion_temps
              implicit none
              integer :: seconde
              integer,parameter :: min2sec = 60
              integer,parameter :: hour2min = 60
              integer,parameter :: day2hour = 24

              print *, "Combien de seconde(s) ?"
              read *, seconde
              print "(A,I0,A)", "Cela fait ",&
                      seconde/min2sec,&
                      " minute(s)"
              print "(A,I0,A)", "soit ", &
                      seconde/(min2sec*hour2min), &
                      " heure(s)"
              print "(A,I0,A)", "soit encore ", &
                      seconde/(min2sec*hour2min*day2hour),&
                      " jour(s)"

      end program
