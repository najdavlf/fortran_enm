
      program test_precision_real
              implicit none
              real :: real_user, sqrt_real_user

              print *, "Entrez un réel :"
              read *, real_user
              sqrt_real_user = sqrt(real_user)
              print *, "Voici votre variable et son carré :"
              print "(F20.4,3X,F20.4)", real_user, real_user**2
              print *, "Voici la différence absolue entre votre",&
                      "variable et le carré de sa racine carrée :" 
              print "(EN10.2E2)", abs(real_user - sqrt_real_user**2)

      end program
