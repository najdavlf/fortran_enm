      program harmonique 

        implicit none
        integer, parameter :: N = 10000000
        real(kind=8) :: somme = 0.0
        integer :: i
    
        print *, ""
        print *, "### Programme harmonique ###"
        print *, ""

        do i = 1,n
          somme = somme + 1.0/i
        end do 
        print *, "La somme harmonique des", N, "premiers entiers vaut :", somme 
        print *, ""

      end program

! Sortie écran à l'exécution :
!
! ### Programme harmonique ###
! 
! La somme harmonique des    10000000 premiers entiers vaut :   16.695311431453085     
! 
!
!real	0m0.070s
!user	0m0.060s
!sys	0m0.010s
!
!
