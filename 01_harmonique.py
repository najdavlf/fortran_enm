#!/usr/bin/python
# -*- coding: latin-1 -*-

# Ce programme calcule la moyenne harmonique 
# d'un nombre N d'entiers et l'affiche

print ""
print "### Programme harmonique ###"
print ""
N = 10000000
somme = 0.

for i in range(1,N):
    somme += 1./i

print "La somme harmonique des",N,"premiers entiers vaut :",somme 
print ""


""" Sortie écran à l'exécution :

### Programme harmonique ###

La somme harmonique des 10000000 premiers entiers vaut : 16.6953112659


real	0m1.605s
user	0m1.490s
sys	0m0.110s

"""
